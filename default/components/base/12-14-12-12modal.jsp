<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="/WEB-INF/jsp/global/header.jsp"%>
<script src="theme/assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="theme/assets/admin/pages/scripts/ui-toastr.js"></script>

<script type="text/javascript"
	src="theme/assets/dualbox/jquery.dualListBox-1.3.min.js"></script>
<%-- <script src="theme/assets/global/plugins/bootbox/bootbox.min.js"
	type="text/javascript"></script> --%>


<!--   <link rel="stylesheet" type="text/css" href="theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css">    -->

<head>
<script type="text/javascript" src="theme/assets/ajax/admin/M-list-users.js"></script>
<%-- <script type="text/javascript" src="theme/assets/ajax/admin/list-users.js"></script>  --%>
<script type="text/javascript">
	function sendResetLink(id){ 
			bootbox.confirm("Are you sure want to reset password?", function(result) {
				if(result==false){
					return;
					}else{
				window.location="sendPassword-admin-ir-user.voziq?userId="+id;
					}
			});
	}
</script>
<script type="text/javascript">
$(document).ready(function(){
	showMentionNotification = function(type, message, title) {
		toastr.options = {
			"closeButton" : true,
			"debug" : false,
			"positionClass" : "toast-top-right",
			"onclick" : null,
			"showDuration" : "1000",
			"hideDuration" : "1000",
			"timeOut" : "5000",
			"extendedTimeOut" : "1000",
			"showEasing" : "swing",
			"hideEasing" : "linear",
			"showMethod" : "fadeIn",
			"hideMethod" : "fadeOut"
		}
		toastr[type](message, title);
	}
mes=$("#message").val();
mestype=$("#msg").val();
<s:if test="%{message!=null}">
showMentionNotification(
			mestype,
			mes,
			"");
	</s:if>
	});
</script>
<!-- --------------------------------------------------------------  form scripts starts------------------------------------------>

	
	
<script language="javascript" type="text/javascript">

            $(function() {
				try{
                $.configureBoxes();
				}catch(e){
					console.log(e);
					console.trace();
				}
            }); 
            
            showMentionNotification = function(type, message, title) {
        		toastr.options = {
        			"closeButton" : true,
        			"debug" : false,
        			"positionClass" : "toast-top-right",
        			"onclick" : null,
        			"showDuration" : "1000",
        			"hideDuration" : "1000",
        			"timeOut" : "5000",
        			"extendedTimeOut" : "1000",
        			"showEasing" : "swing",
        			"hideEasing" : "linear",
        			"showMethod" : "fadeIn",
        			"hideMethod" : "fadeOut"
        		}
        		toastr[type](message, title);
        	}
            
          
            
            
            $( document ).on( 'focus', ':input', function(){
			        $( this ).attr( 'autocomplete', 'off' );
			    });
				$( document ).on( 'focus', ':password', function(){
			        $( this ).attr( 'autocomplete', 'new-password' );
			    });
			
					 
	  		  			
            $(document).ready(function() {
  				
            	
             	
  			
  			 	$("#modal-scroll").on('scroll touchmove mousewheel', function(e){ 
  					e.stopPropagation();
  				
  					var drop = $(".select2-container.select2-container--default");
	  		  		if (drop.hasClass('select2-container--open')) {
  		  
	  		  		drop.removeClass('select2-container--open')
	        	       .addClass('select2-container--close')
  		 			 } 
	  		  		
	  		  		
  				});	
  			
  			 	
  			 	
  			 	
  			 	
  			 	
  			 	$(".select2-selection.select2-selection--single").on('click', function(e){ 
  			
  			 		var drop = $(".select2-container.select2-container--default");
	  		  	 if (drop.hasClass('select2-container--close')) {
  	  		 	  		  
   		  		  		drop.removeClass('select2-container--close')
   		        	       .addClass('select2-container--open')
   	  		 			 } 
	  		  		
  				});	
  				
  		  		 		  	
  			

  				
  				var $loading = $('#loader').hide();
		          //Attach the event handler to any element
		          $(document)
		            .ajaxStart(function () {
		               //ajax request went so show the loading image
		                $loading.show();
		            })
		          .ajaxStop(function () {
		              //got response so hide the loading image
		               $loading.hide();
		           });
		         

      $("#resetSave").click(function() {
            		var $Password=$("#PassR").val();
              	   var $Retypepassword=$("#RepassR").val();
              	 var status=true;
              	var pass =/^(?=.*[!@#$%^&*])(?=.*[a-zA-Z])[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};â:â\\|,.<>\/?]{6,32}$/;
              	if($Password == "")
            	   {
            	   $("#Reset-Pass-word").addClass("has-error");
            	   $("#Reset-PasswordHelp").html("Enter Password");
            	   status = false;
            	   }else if($Password.trim().length < 6 || $Password.trim().length > 32){
                  	   $("#Reset-Pass-word").addClass("has-error");
            			$("#Reset-PasswordHelp").html("Password length must between 6-32 characters !");					
            			status=false;
                }
                      else if( !pass.test( $Password.trim() ) ){
            			$("#Reset-Pass-word").addClass("has-error");
            			$("#Reset-PasswordHelp").html("Password contains at least one special character & one character");					
            			status=false;
            		}else
            		   {
             		   $("#Reset-PasswordHelp").html("");
             		   }
              	if($Retypepassword == "")
            	   {
            	   $("#Reset-Reenterpass-word").addClass("has-error");
            	   $("#Reset-RePasswordHelp").html("Enter Re-type Password");
            	   status = false;
            	   }
            else if($Password!=$Retypepassword)
            {
            	   $("#Reset-Reenterpass-word").addClass("has-error");
            	   $("#Reset-RePasswordHelp").html("Password and Re-type Password should be same");
            status = false;
            }
            else
            	   {
            	   $("#Reset-RePasswordHelp").html("");
            	   }
    
             	if(status == true){        		   
             	  	
             	   	ResetSave();
             
             		   }
              	return status;
    });     	
         
            	
            	
         	   function validateEmail($email) {
  				  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  				  if( !emailReg.test( $email ) ) {
  				    return false;
  				  } else {
  				    return true;
  				  }
  				}
           	$("#save1").click(function(){   
           		
          

         	   var $Firstname=$("#FName").val();
         	   var $Lastname=$("#LName").val();
         	   var $Email=$("#email").val();
         	   var $user_Id=$("#user-Id").val();
         	   var $userIdAsEmail=$("#userIdAsEmail");
         	   var $Password=$("#Pass").val();
         	   var $Retypepassword=$("#Repass").val();
         	   var $Timezone=$("#timeZone").val();
         	   var $userId=$("#userId").val();
         	  // var pass = /^.*(?=.{6,32})(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z0-9!@#$%]+$/;
         	   //var pass =/^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-zA-Z])[a-zA-Z0-9!@#$%^&*]{6,32}$/;
         	  var pass =/^(?=.*[!@#$%^&*])(?=.*[a-zA-Z])[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};â??:â?\\|,.<>\/?]{6,32}$/;
         	
         	// $('#save-adminiruser').attr('action', 'saveadminiruser.voziq');
         	   var status=true;
         	   if($Firstname == "")
         		   {
         		   $("#firstName").addClass("has-error");
         		   $("#FNameHelp").html("Enter FirstName");
         		   status = false;
         		   }else
         			   {
         			   $("#FNameHelp").html("");
         			  $("#firstName").removeClass("has-error");
         			   }
         	   if($Lastname=="")
         		   {
         		   $("#lastName").addClass("has-error");
         		   $("#LNameHelp").html("Enter LastName");
         		   status = false;
         		   }
         	   else
         		   {
         		   $("#LNameHelp").html("");
         		  $("#LNameHelp").removeClass("has-error");
         		   }
         	  if(!document.getElementById("userIdAsEmail").checked)
    		   {
         		 $userIdAsEmail.val(false);
         	   if($user_Id == "")
         		   {
         		   $("#user-IdBlock").addClass("has-error");
         		   $("#LoginIdHelp").html("Enter LoginId");
         		   status = false;
         		   }
         	   /* else if (!validateEmail($user_Id)) {
         			   $("#user-IdBlock").addClass("has-error");
         			   $("#LoginIdHelp").html("Enter valid User ID");
         			   status = false;
 				} */ 
         	   else
         		   {
         		   $("#LoginIdHelp").html("");
         		  $("#user-IdBlock").removeClass("has-error");
         		   }}else{
         			  $userIdAsEmail.val(true);
         		   }
         	  
         		  
         	  if($Email == "")
    		   {
    		   $("#e-Mail").addClass("has-error");
    		   $("#EMailHelp").html("Enter Email");
    		   status = false;
    		   }else if (!validateEmail($Email)) {
    			   $("#e-Mail").addClass("has-error");
    			   $("#EMailHelp").html("Enter Valid Email ID");
    			   status = false;
			}
    	   else
    		   {
    		   $("#EMailHelp").html("");
    		  $("#e-Mail").removeClass("has-error");
    		   } 
         		   
         	 /*  if($userId==0)
         		   {
         	   if($Password == "")
         		   {
         		   $("#Pass-word").addClass("has-error");
         		   $("#PasswordHelp").html("Enter Password");
         		   status = false;
         		   }
         	   else if($Password.trim().length < 6 || $Password.trim().length > 32){
 					$("#Pass-word").addClass("has-error");
 					$("#PasswordHelp").text("Password length must between 6-32 characters !");					
 					status=false;
 				}
         	   else
         		   {
         		   $("#PasswordHelp").html("");
         		   }        		   
         	   if($Retypepassword == "")
         		   {
         		   $("#Reenterpass-word").addClass("has-error");
         		   $("#RePasswordHelp").html("Enter Re-type Password");
         		   status = false;
         		   }
         	   else if($Password!=$Retypepassword)
     		   {
         		   $("#Reenterpass-word").addClass("has-error");
     		   	   $("#RePasswordHelp").html("Password and Re-type Password should not match");
     		   status = false;
     		   }
         	   else
         		   {
         		   $("#RePasswordHelp").html("");
         		   }
         		   }*/ 
         		   
         		  if($userId==0)
        		   {
        	   if($Password == "")
        		   {
        		   $("#Pass-word").addClass("has-error");
        		   $("#PasswordHelp").html("Enter Password");
        		   status = false;
        		   }
        	  else if($Password.trim().length < 6 || $Password.trim().length > 32){
          	   $("#Pass-word").addClass("has-error");
					$("#PasswordHelp").html("Password Length Must Between 6-32 Characters !");					
					status=false;
             }
	               else if( !pass.test( $Password.trim() ) ){
					$("#Pass-word").addClass("has-error");
					$("#PasswordHelp").html("Password Contains At Least One Special Character & One Character");					
					status=false;
				}
        	   else
        		   {
        		   $("#PasswordHelp").html("");
        		   }        		   
        	   if($Retypepassword == "")
        		   {
        		   $("#Reenterpass-word").addClass("has-error");
        		   $("#RePasswordHelp").html("Enter Re-type Password");
        		   status = false;
        		   }
        	   else if($Password!=$Retypepassword)
    		   {
        		   $("#Reenterpass-word").addClass("has-error");
    		   	   $("#RePasswordHelp").html("Password and Re-type Password Should be Same");
    		   status = false;
    		   }
        	   else
        		   {
        		   $("#RePasswordHelp").html("");
        		   }
        		   }
         		   
         	   if($Timezone == null)
         		   {
         		   $("#timeZone-err").addClass("has-error");
         		   $("#TimezoneHelp").html("Enter Timezone");
         		   status = false;
         		   }
         	   else
         		   {
         		   $("#TimezoneHelp").html("");
         		  $("#timeZone-err").removeClass("has-error");
         		   }
        	
         
         	  // alert(status);
         	   	if(status == true){        		   
         	  	
         	   	addRelod_M();
         
         		   }else{    

         				// $("#modal-scroll").scrollTop(0,0);
         				    $('#modal-scroll').animate({ scrollTop: 0 }, 'slow');
         				return status;
          		   }
            });
           	mes=$("#message").val();
           	mesType=$("#msg").val();
           	<s:if test="%{message!=null}">
        	showMentionNotification(
        			mesType,
        			mes,
        			"");
        	</s:if>        	
            }); 
            
        
      
            
            function toggleCheckbox(element)
       	 {       		   
       		if(element.checked)
       			{
       			$("#user-IdBlock").hide();
       			}else
       				{
       				$("#user-IdBlock").show();
       				}       	  
       	 }
       
         
   
      
           </script>
<!-- --------------------------------------------------------------  form scripts ends ------------------------------------------>



<style>



/* #XAdd{
padding-top: 20px;
 padding-right: 0px !important;
}
 */



.fa-times{
  color: #b2b1c5;
}
.fa-times:hover{
  color:red;
}

	         /* RESET FORM form starts here  */


/* 
#X{
    float: right;
    padding-top: 18px;
    height: 20px;
   width: 30px;
   }
     */
/*     .form .form-actions{
padding: 0px !important;
}
 */

/* #loader{
position: fixed; 
left: 50%; 
top: 50%;  


} */

/* .portPad{
margin-bottom: 5px !important;
} */

/* Add Zoom Animation */
.animateModal {
  -webkit-animation: animatezoom 0.6s;
  animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
  from {-webkit-transform: scale(0)} 
  to {-webkit-transform: scale(1)}
}
  
@keyframes animatezoom {
  from {transform: scale(0)} 
  to {transform: scale(1)}
}

 div#m_modal_add {
    /* height: 550px;*/
    top: calc(40% - 200px) !important; 
   
} 
/*   */
/* #m_modal_add .modal-dialog {
    -webkit-transform: translate(0,-50%);
    -o-transform: translate(0,-50%);
    transform: translate(0,-50%);
    top: 40%;
    margin: 0 auto;
}
 */
	/* 	 @media (min-width: 1024px) {
		 .modal{
		  top: calc(40% - 200px) !important; 
		 max-width: 1000px;
		 }
		 
      .modal-lg {
        width: 90%;
       max-width:1200px;
      }
    } */ 
 /* */

 .close:before {
   font-size: .1 rem !important; 
}
 #heading{
  color: #6c757d;
 }

</style>



</head>
<s:actionerror />

		
									
							


	<div class="m-grid__item m-grid__item--fluid m-wrapper">
	

					<div class="m-content">
									<div class="m-portlet m-portlet--mobile">
								 <s:hidden name="message" id="message" ></s:hidden>
						         <s:hidden name="msg" id="msg" ></s:hidden>
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
									<span class="m-portlet__head-icon">
													<i class="fa fa-users"></i>
												</span>
										<h3 class="m-portlet__head-text m--font-transform-u">
											USERS
										</h3>
									</div>
								</div>
<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item" >
                                  <span>Status: </span>&nbsp;&nbsp;&nbsp;&nbsp;
                                  <s:select name="status" list="userStatus"
											id="stauts" listKey="value"	listValue="name"
											theme="simple" data-live-search="true"
											onchange="listDatatableUsersStatus(this.value)"
											cssClass="col-lg-12 form-control m-bootstrap-select m_selectpicker"/>
								</li>
							</ul>
						</div>
						
						
						
								<div class="m-portlet__head-tools">
													<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
<!--####### opened modal form ###### -->
									<s:if test="%{#session.add=='true'}">
									<li class="m-nav__item m-nav__item--home">
 						 	 <a  class="m-nav__link m-nav__link--icon" data-backdrop="static" data-keyboard="false"  onclick="javascript:return modalOpen();" ><i class="m-nav__link-icon fas fa-plus"></i></a> 
						<!--	<a  class="m-nav__link m-nav__link--icon" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#m_modal_add" ><i class="m-nav__link-icon fas fa-plus"></i></a> 
							 --></li>
									<li class="m-nav__item m-nav__item--home">
								 <a href="new-admin-ir-user.voziq" class="m-nav__link m-nav__link--icon" ><i class="m-nav__link-icon fas fa-plus"></i></a> 
									</li>
								
									
									</s:if>
<!--####### edit through modal form  ###### -->									
									<s:if test="%{#session.edit=='true'}">
									<li class="m-nav__item m-nav__item--home1243">
										<a href="#" class="m-nav__link m-nav__link--icon" data-backdrop="static" data-keyboard="false"  onclick="javascript:editReload_M();"><i class="m-nav__link-icon fas fa-pencil-alt"></i></a>
									</li>
									<li class="m-nav__item m-nav__item--home1243">
										<a href="#" class="m-nav__link m-nav__link--icon" onclick="javascript:editUser_M();"><i class="m-nav__link-icon fas fa-pencil-alt"></i></a>
									</li>
									<li class="m-nav__item m-nav__item--home1243">
										<a href="#" class="m-nav__link m-nav__link--icon" onclick="javascript:editUser();"><i class="m-nav__link-icon fas fa-pencil-alt"></i></a>
									</li>
									</s:if>
<!--####### delete through modal form  ###### -->
									<s:if test="%{#session.delete=='true'}">
									<li class="m-nav__item m-nav__item--home1243">
										<a href="#" class="m-nav__link m-nav__link--icon" onclick="javascript:deleteUser_ajax();"><i class="m-nav__link-icon fas fa-trash-alt"></i></a>
									</li>
								<li class="m-nav__item m-nav__item--home1243">
										<a href="#" class="m-nav__link m-nav__link--icon" onclick="javascript:deleteUser();"><i class="m-nav__link-icon fas fa-trash-alt"></i></a>
									</li> 
								</s:if>
								</ul>
								</div>
							</div>
							<div class="m-portlet__body">
								<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="example">
									<thead>
										<tr>
										<th></th>
										<th>S.No</th>
											<th>User Name</th>
											<th>Email</th>
											<th>Login Id</th>
											<th>Reset Password</th>
											<th>Status</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				
				
 <div id="loader" >  Loading <img alt="loading Data" src="theme/assets/global/img/loading.gif"  ></div>  				
				
				
				
				
 </div> 
	</div> </div>
				


 

				<!--begin::Modal-->
						<div class="modal fade" id="m_modal_add"  role="dialog" 	aria-labelledby="exampleModalLabel" aria-hidden="true"  style=" overflow: auto !important;" ><!-- style="width: 56%; margin-left: -28%;" -->
						
						
						
							<div class="modal-dialog modal-lg " 	 role="document"  style="  display:contents !important; position: relative !important;  max-width: 1017px;  pointer-events: none !important;"> <!-- style="width: 56%;  margin: auto;" -->
	


					<div class="modal-content"  > <!-- style="width: auto; margin: 0px !important; max-width: 100%;" -->
							<div class="m-portlet m-portlet--mobile portPad">	
									 <div class="modal-header">
										
								<div style=" color  : #b2b1c5;">
									 
									<h3 class="modal-title m-portlet__head-text m--font-transform-u" id="exampleModalLabel">
									<span class="m-portlet__head-icon" style="  color: #b2b1c5;"> <i	class="fas fa-user-plus icon-to-change"></i>
									</span><label  id="heading">ADD New User</label>
										</h3></div>
								<!-- </div>
								 </div> -->
								 <div id="XAdd" class="" style="float:right !important; "><a  onclick="javascript:refreshForm();"><i class="fas fa-times fa-3x" ></i></a> </div>
							<!-- <button type="button"  class="close" data-dismiss="modal" >
										<i class="fas fa-times fa-3x"   onclick="javascript:refreshForm();" ></i>
										</button> -->
						<!-- </div> -->
										
										
									</div>
								

							
									
									
									<div class="modal-body"  style="padding: 0px 0px 0px 0px;">
												
					<div class="form">        
				<section class="form-elegant scrollbar-light-blue">	
			
						
			
			
						<s:form 
							cssClass="form-horizontal form-bordered form-row-stripped "
							theme="simple" id="save-adminiruser" >
			<!-- <div class="m-scrollable m-scroller ps ps--active-y" data-scrollbar-shown="true" data-scrollable="true" data-height="400px"
	 style="
	 flex: 1 0 auto;
    flex-wrap: wrap;
    display: flex;
    padding: 25px;
    min-height: 100px;
    right: 2%;
   
    height: 700px;
       max-height: calc(100vh - 200px);
    overflow-y: auto;
        PADDING: 0PX 25PX 0PX 26PX;"> -->		<div class=""  data-height="400px"
	 style="

   
    height: 400px;
       max-height: calc(100vh - 200px);
    overflow-y:scroll;
    overflow-x:auto; 
    
        PADDING: 0PX 25PX 0PX 13PX;">		
							
					<!--  value="%{userId}" -->
						<s:hidden name="userId" value="%{userId}" id="userId" />
					
							<s:hidden name="status" value="%{users.status}"></s:hidden>
							<s:hidden name="accountId" value="%{users.accountId}"></s:hidden>
							<s:hidden name="name"  value="%{name}"></s:hidden>
							<s:hidden name="admin" value="%{users.admin}"></s:hidden>
							<s:hidden name="message" id="message"></s:hidden>
							<s:hidden name="msg" id="msg"></s:hidden>

			
			
							<div class="m-portlet__body form">
								<div class="form-group m-form__group row" id="firstName">
									<label class="col-lg-2 control-label">First Name<span
										class="required" style="color: red;"> * </span></label>
									<div class="col-lg-4">
										<s:textfield theme="simple" id="FName" name="firstNm"
											value="%{users.firstNm}" cssClass="form-control m-input" />
										<span class="m-form__help" id="FNameHelp" style="color: red;"></span>
									</div>
								</div>
								<div class="form-group m-form__group row" id="lastName">
									<label class="col-lg-2 control-label">Last Name<span
										class="required" style="color: red;"> * </span></label>
									<div class="col-lg-4">
										<s:textfield theme="simple" id="LName" name="lastNm"
											value="%{users.lastNm}" cssClass="form-control m-input" />
										<span class="m-form__help" id="LNameHelp" style="color: red;"></span>
									</div>
								</div>
								<div class="form-group m-form__group row" id="e-Mail">
									<label class="col-lg-2 control-label">Email<span
										class="required" style="color: red;"> * </span></label>
									<div class="col-lg-4">
										<s:if test="%{userId ==0}" >
											<s:textfield theme="simple" id="email" name="email"
												value="%{users.email}" cssClass="form-control m-input" />
											<span class="m-form__help" id="EMailHelp" style="color: red;"></span>
										</s:if>
										<s:else>
											<s:textfield theme="simple" id="email" readonly="true"
												name="email" value="%{users.email}"
												cssClass="form-control m-input" />
											<span class="m-form__help" id="EMailHelp" style="color: red;"></span>
										</s:else>
									</div>
								</div>
						 		<s:if test="%{userId ==0}" >
									<div class="form-group m-form__group row"
										id="userIdAsEmailBlock">
										<label class="col-lg-2 control-label">Login ID Same
											as Email</label>
										<div class="col-lg-10">
											<label class="m-checkbox col-lg-10"> <s:checkbox
													theme="simple" id="userIdAsEmail" name="userIdAsEmail"
													cssClass="m-checkbox" fieldValue="false"
													onchange="toggleCheckbox(this)" /> <span></span>
											</label>
										</div>
									</div>
								 </s:if>
							
 
							
								<div class="form-group m-form__group row" id="user-IdBlock">
									<label class="col-lg-2 control-label">Login ID<span
										class="required" style="color: red;"> * </span></label>
									<div class="col-lg-4">
										<s:if test="%{userId ==0}">
										<input id="356" type="password"  style="display:none"/>
											<s:textfield theme="simple" id="user-Id" name="loginId"
												value="%{users.loginId}" cssClass="form-control m-input" />
											<span class="m-form__help" id="LoginIdHelp"
												style="color: red;"></span>
										</s:if>
										<s:else>
								
											<s:textfield theme="simple" id="user-Id" readonly="true"
												name="loginId" value="%{users.loginId}"
												cssClass="form-control m-input" />
											<span class="m-form__help" id="LoginIdHelp"
												style="color: red;"></span>
										</s:else>
									</div>
								</div>
								
	
								<s:if test="authType != 'ldap'">
									<s:if test="%{userId ==0}">
									
						
				 
									
									<div id="passHide">
										<div class="form-group m-form__group row" id="Pass-word">
											<label class="col-lg-2 control-label">New Password<span
												class="required" style="color: red;"> * </span></label>
											<div class="col-lg-4">
		
												<s:password theme="simple" id="Pass" name="password"
													cssClass="form-control m-input"  />
												<span class="m-form__help" id="PasswordHelp"
													style="color: red;"></span>
											</div>
										</div>
										<div class="form-group m-form__group row"
											id="Reenterpass-word">
											<label class="col-lg-2 control-label">Re-type New
												Password<span class="required" style="color: red;"> *
											</span>
											</label>
											<div class="col-lg-4">
										
												<s:password theme="simple" id="Repass"
													name="reenterpassword" cssClass="form-control m-input" />
												<span class="m-form__help" id="RePasswordHelp"
													style="color: red;"></span>
											</div>
										</div>
										</div>
									</s:if>
								</s:if>

								<div class="form-group m-form__group row" id="timeZone-err">
									<label class="col-lg-2 control-label">Time Zone<span
										class="required" style="color: red;"> * </span></label>
									<div class="col-lg-4" >
										<select name="timeZone" id="timeZone"
											class="form-control m-select2">
											<option value="UTC">(GMT+00:00) UTC</option>
											<option value="Hawaii">(GMT-10:00) Hawaii</option>
											<option value="Alaska">(GMT-09:00) Alaska</option>
											<option value="Pacific Time (US &amp; Canada)">(GMT-08:00)
												Pacific Time (US &amp; Canada)</option>
											<option value="Arizona">(GMT-07:00) Arizona</option>
											<option value="Mountain Time (US &amp; Canada)">(GMT-07:00)
												Mountain Time (US &amp; Canada)</option>
											<option value="Central Time (US &amp; Canada)">(GMT-06:00)
												Central Time (US &amp; Canada)</option>
											<option value="Eastern Time (US &amp; Canada)">(GMT-05:00)
												Eastern Time (US &amp; Canada)</option>
											<option value="Indiana (East)">(GMT-05:00) Indiana
												(East)</option>
											<option value="" disabled="disabled">-------------</option>
											<option value="International Date Line West">(GMT-11:00)
												International Date Line West</option>
											<option value="Midway Island">(GMT-11:00) Midway
												Island</option>
											<option value="Samoa">(GMT-11:00) Samoa</option>
											<option value="Tijuana">(GMT-08:00) Tijuana</option>
											<option value="Chihuahua">(GMT-07:00) Chihuahua</option>
											<option value="Mazatlan">(GMT-07:00) Mazatlan</option>
											<option value="Central America">(GMT-06:00) Central
												America</option>
											<option value="Guadalajara">(GMT-06:00) Guadalajara</option>
											<option value="Mexico City">(GMT-06:00) Mexico City</option>
											<option value="Monterrey">(GMT-06:00) Monterrey</option>
											<option value="Saskatchewan">(GMT-06:00)
												Saskatchewan</option>
											<option value="Bogota">(GMT-05:00) Bogota</option>
											<option value="Lima">(GMT-05:00) Lima</option>
											<option value="Quito">(GMT-05:00) Quito</option>
											<option value="Caracas">(GMT-04:30) Caracas</option>
											<option value="Atlantic Time (Canada)">(GMT-04:00)
												Atlantic Time (Canada)</option>
											<option value="La Paz">(GMT-04:00) La Paz</option>
											<option value="Santiago">(GMT-04:00) Santiago</option>
											<option value="Newfoundland">(GMT-03:30)
												Newfoundland</option>
											<option value="Brasilia">(GMT-03:00) Brasilia</option>
											<option value="Buenos Aires">(GMT-03:00) Buenos
												Aires</option>
											<option value="Georgetown">(GMT-03:00) Georgetown</option>
											<option value="Greenland">(GMT-03:00) Greenland</option>
											<option value="Mid-Atlantic">(GMT-02:00)
												Mid-Atlantic</option>
											<option value="Azores">(GMT-01:00) Azores</option>
											<option value="Cape Verde Is.">(GMT-01:00) Cape
												Verde Is.</option>
											<option value="Casablanca">(GMT+00:00) Casablanca</option>
											<option value="Dublin">(GMT+00:00) Dublin</option>
											<option value="Edinburgh">(GMT+00:00) Edinburgh</option>
											<option value="Lisbon">(GMT+00:00) Lisbon</option>
											<option value="London">(GMT+00:00) London</option>
											<option value="Monrovia">(GMT+00:00) Monrovia</option>
											<option value="Amsterdam">(GMT+01:00) Amsterdam</option>
											<option value="Belgrade">(GMT+01:00) Belgrade</option>
											<option value="Berlin">(GMT+01:00) Berlin</option>
											<option value="Bern">(GMT+01:00) Bern</option>
											<option value="Bratislava">(GMT+01:00) Bratislava</option>
											<option value="Brussels">(GMT+01:00) Brussels</option>
											<option value="Budapest">(GMT+01:00) Budapest</option>
											<option value="Copenhagen">(GMT+01:00) Copenhagen</option>
											<option value="Ljubljana">(GMT+01:00) Ljubljana</option>
											<option value="Madrid">(GMT+01:00) Madrid</option>
											<option value="Paris">(GMT+01:00) Paris</option>
											<option value="Prague">(GMT+01:00) Prague</option>
											<option value="Rome">(GMT+01:00) Rome</option>
											<option value="Sarajevo">(GMT+01:00) Sarajevo</option>
											<option value="Skopje">(GMT+01:00) Skopje</option>
											<option value="Stockholm">(GMT+01:00) Stockholm</option>
											<option value="Vienna">(GMT+01:00) Vienna</option>
											<option value="Warsaw">(GMT+01:00) Warsaw</option>
											<option value="West Central Africa">(GMT+01:00) West
												Central Africa</option>
											<option value="Zagreb">(GMT+01:00) Zagreb</option>
											<option value="Athens">(GMT+02:00) Athens</option>
											<option value="Bucharest">(GMT+02:00) Bucharest</option>
											<option value="Cairo">(GMT+02:00) Cairo</option>
											<option value="Harare">(GMT+02:00) Harare</option>
											<option value="Helsinki">(GMT+02:00) Helsinki</option>
											<option value="Istanbul">(GMT+02:00) Istanbul</option>
											<option value="Jerusalem">(GMT+02:00) Jerusalem</option>
											<option value="Kyev">(GMT+02:00) Kyev</option>
											<option value="Minsk">(GMT+02:00) Minsk</option>
											<option value="Pretoria">(GMT+02:00) Pretoria</option>
											<option value="Riga">(GMT+02:00) Riga</option>
											<option value="Sofia">(GMT+02:00) Sofia</option>
											<option value="Tallinn">(GMT+02:00) Tallinn</option>
											<option value="Vilnius">(GMT+02:00) Vilnius</option>
											<option value="Baghdad">(GMT+03:00) Baghdad</option>
											<option value="Kuwait">(GMT+03:00) Kuwait</option>
											<option value="Moscow">(GMT+03:00) Moscow</option>
											<option value="Nairobi">(GMT+03:00) Nairobi</option>
											<option value="Riyadh">(GMT+03:00) Riyadh</option>
											<option value="St. Petersburg">(GMT+03:00) St.
												Petersburg</option>
											<option value="Volgograd">(GMT+03:00) Volgograd</option>
											<option value="Tehran">(GMT+03:30) Tehran</option>
											<option value="Abu Dhabi">(GMT+04:00) Abu Dhabi</option>
											<option value="Baku">(GMT+04:00) Baku</option>
											<option value="Muscat">(GMT+04:00) Muscat</option>
											<option value="Tbilisi">(GMT+04:00) Tbilisi</option>
											<option value="Yerevan">(GMT+04:00) Yerevan</option>
											<option value="Kabul">(GMT+04:30) Kabul</option>
											<option value="Ekaterinburg">(GMT+05:00)
												Ekaterinburg</option>
											<option value="Islamabad">(GMT+05:00) Islamabad</option>
											<option value="Karachi">(GMT+05:00) Karachi</option>
											<option value="Tashkent">(GMT+05:00) Tashkent</option>
											<option value="Chennai">(GMT+05:30) Chennai</option>
											<option value="Kolkata">(GMT+05:30) Kolkata</option>
											<option value="Mumbai">(GMT+05:30) Mumbai</option>
											<option value="New Delhi">(GMT+05:30) New Delhi</option>
											<option value="Sri Jayawardenepura">(GMT+05:30) Sri
												Jayawardenepura</option>
											<option value="Kathmandu">(GMT+05:45) Kathmandu</option>
											<option value="Almaty">(GMT+06:00) Almaty</option>
											<option value="Astana">(GMT+06:00) Astana</option>
											<option value="Dhaka">(GMT+06:00) Dhaka</option>
											<option value="Novosibirsk">(GMT+06:00) Novosibirsk</option>
											<option value="Rangoon">(GMT+06:30) Rangoon</option>
											<option value="Bangkok">(GMT+07:00) Bangkok</option>
											<option value="Hanoi">(GMT+07:00) Hanoi</option>
											<option value="Jakarta">(GMT+07:00) Jakarta</option>
											<option value="Krasnoyarsk">(GMT+07:00) Krasnoyarsk</option>
											<option value="Beijing">(GMT+08:00) Beijing</option>
											<option value="Chongqing">(GMT+08:00) Chongqing</option>
											<option value="Hong Kong">(GMT+08:00) Hong Kong</option>
											<option value="Irkutsk">(GMT+08:00) Irkutsk</option>
											<option value="Kuala Lumpur">(GMT+08:00) Kuala
												Lumpur</option>
											<option value="Perth">(GMT+08:00) Perth</option>
											<option value="Singapore">(GMT+08:00) Singapore</option>
											<option value="Taipei">(GMT+08:00) Taipei</option>
											<option value="Ulaan Bataar">(GMT+08:00) Ulaan
												Bataar</option>
											<option value="Urumqi">(GMT+08:00) Urumqi</option>
											<option value="Osaka">(GMT+09:00) Osaka</option>
											<option value="Sapporo">(GMT+09:00) Sapporo</option>
											<option value="Seoul">(GMT+09:00) Seoul</option>
											<option value="Tokyo">(GMT+09:00) Tokyo</option>
											<option value="Yakutsk">(GMT+09:00) Yakutsk</option>
											<option value="Adelaide">(GMT+09:30) Adelaide</option>
											<option value="Darwin">(GMT+09:30) Darwin</option>
											<option value="Brisbane">(GMT+10:00) Brisbane</option>
											<option value="Canberra">(GMT+10:00) Canberra</option>
											<option value="Guam">(GMT+10:00) Guam</option>
											<option value="Hobart">(GMT+10:00) Hobart</option>
											<option value="Melbourne">(GMT+10:00) Melbourne</option>
											<option value="Port Moresby">(GMT+10:00) Port
												Moresby</option>
											<option value="Sydney">(GMT+10:00) Sydney</option>
											<option value="Vladivostok">(GMT+10:00) Vladivostok</option>
											<option value="Magadan">(GMT+11:00) Magadan</option>
											<option value="New Caledonia">(GMT+11:00) New
												Caledonia</option>
											<option value="Solomon Is.">(GMT+11:00) Solomon Is.</option>
											<option value="Auckland">(GMT+12:00) Auckland</option>
											<option value="Fiji">(GMT+12:00) Fiji</option>
											<option value="Kamchatka">(GMT+12:00) Kamchatka</option>
											<option value="Marshall Is.">(GMT+12:00) Marshall
												Is.</option>
											<option value="Wellington">(GMT+12:00) Wellington</option>
											<option value="Nuku'alofa">(GMT+13:00) Nuku'alofa</option>
										</select> <span class="m-form__help" id="TimezoneHelp"
											style="color: red;"></span>
										<s:hidden value="%{users.timeZone}" id="setTimeZone" />
										<script type="text/javascript">                      	              
															var timeZone1 = $("#setTimeZone").val();
															$("#timeZone").val(timeZone1.toString());
		   											</script>
									</div>
								</div>
								<div class="form-group m-form__group row" id="roles1">
									<label class="col-lg-2 control-label">Report Groups
									<!-- <span class="required" style="color: red;"> * </span> -->
										</label>
									<div class="col-lg-4" align="left">
										<span class="m-form__help" id="roleHelp"
											style="color: red; align: center;"></span>
										<table
											class="form-horizontal form-bordered form-label-stripped">
											<tr>
												<td>
													<div class="mws-dualbox clearfix">
														<div class="mws-dualbox-col1">
															<div class="mws-dualbox-filter clearfix">
																<p align="center">
																	<label for="box1Filter" align="center">Available
																		Report Groups</label> <input id="box1Filter"
																		class="form-control" type="text">
															</div>
															<s:select id="box1View" list="listReportGroup1"
																listKey="id" listValue="name" name="roleId"
																cssClass="form-control" multiple="true" size="15"
																cssStyle="height:250px;width:375px;" />
															<p align="center">
																<span id="box1Counter" class="countLabel"></span>
															</p>
															<select style="display: none;" id="box1Storage"></select>
														</div>
												</td>
												<td style="padding-top: 5%">
													<div class="mws-dualbox-col2">
														<button id="to2" type="button" title="Move Selected Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-right"></i>
														</button>
														<br /> <br />
														<button id="allTo2" type="button" title="Move All Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-double-right"></i>
														</button>
														<br /> <br />
														<div class="clear"></div>
														<button id="to1" type="button" title="Move Selected Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-left"></i>
														</button>
														<br /> <br />
														<button id="allTo1" type="button" title="Move All Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-double-left"></i>
														</button>
													</div>
												</td>
												<td>
													<div class="mws-dualbox clearfix">
														<div class="mws-dualbox-col1">
															<div class="mws-dualbox-filter clearfix">
																<p align="center">
																	<label for="box2Filter"> Assigned Report Groups</label>
																	<input id="box2Filter" class="form-control" type="text">
															</div>
															<s:select id="box2View" list="listReportGroup2"
																listKey="id" listValue="name" name="reportGroupIds"
																cssClass="form-control" multiple="true" size="15"
																cssStyle="height:250px;width:375px;" />
															<p align="center">
																<span id="box2Counter" class="countLabel"></span>
															</p>
															<select style="display: none;" id="box2Storage"></select>
														</div>
													</div>
													</div>
												</td>
											</tr>
										</table>
									</div>
								</div>
								<div class="form-group m-form__group row" id="roles2">
									<label class="col-lg-2 control-label">Dashboard Groups
									<!-- <span class="required" style="color: red;"> * </span> -->
									</label>
									<div class="col-lg-4" align="left">
										<span class="m-form__help" id="roleHelp"
											style="color: red; align: center;"></span>
										<table
											class="form-horizontal form-bordered form-label-stripped">
											<tr>
												<td>
													<div class="mws-dualbox clearfix">
														<div class="mws-dualbox-col1">
															<div class="mws-dualbox-filter clearfix">
																<p align="center">
																	<label for="box3Filter" align="center">Available
																		Dashboard Groups</label> <input id="box3Filter"
																		class="form-control" type="text">
															</div>
															<s:select id="box3View" list="listInsightGroup1"
																listKey="id" listValue="igName" name="roleId"
																cssClass="form-control" multiple="true" size="15"
																cssStyle="height:250px;width:375px;" />
															<p align="center">
																<span id="box3Counter" class="countLabel"></span>
															</p>
															<select style="display: none;" id="box3Storage"></select>
														</div>
												</td>
												<td style="padding-top: 5%">
													<div class="mws-dualbox-col2">
														<button id="to4" type="button" title="Move Selected Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-right"></i>
														</button>
														<br /> <br />
														<button id="allTo4" type="button" title="Move All Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-double-right"></i>
														</button>
														<br /> <br />
														<div class="clear"></div>
														<button id="to3" type="button" title="Move Selected Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-left"></i>
														</button>
														<br /> <br />
														<button id="allTo3" type="button" title="Move All Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-double-left"></i>
														</button>
													</div>
												</td>
												<td>
													<div class="mws-dualbox clearfix">
														<div class="mws-dualbox-col1">
															<div class="mws-dualbox-filter clearfix">
																<p align="center">
																	<label for="box4Filter"> Assigned Dashboard
																		Groups</label> <input id="box4Filter" class="form-control"
																		type="text">
															</div>
															<s:select id="box4View" list="listInsightGroup2"
																name="insightGroupIds" listKey="id" listValue="igName"
																cssClass="form-control" multiple="true" size="15"
																cssStyle="height:250px;width:375px;" />
															<p align="center">
																<span id="box4Counter" class="countLabel"></span>
															</p>
															<select style="display: none;" id="box4Storage"></select>
														</div>
													</div>
													</div>
												</td>
											</tr>
										</table>
									</div>
								</div>
								<div class="form-group m-form__group row" id="roles4">
									<label class="col-lg-2 control-label">Insights Data	Sources Groups
									<!-- <span class="required" style="color: red;">	* </span> -->
									</label>
									<div class="col-lg-4" align="left">
										<span class="m-form__help" id="roleHelp"
											style="color: red; align: center;"></span>
										<table
											class="form-horizontal form-bordered form-label-stripped">
											<tr>
												<td>
													<div class="mws-dualbox clearfix">
														<div class="mws-dualbox-col1">
															<div class="mws-dualbox-filter clearfix">
																<p align="center">
																	<label for="box7Filter" align="center">Available
																		Insights Data Sources Groups</label> <input id="box7Filter"
																		class="form-control" type="text">
															</div>
															<s:select id="box7View"
																list="listInsightDatasourceGroup1" listKey="id"
																listValue="dgName" name="roleId" cssClass="form-control"
																multiple="true" size="15"
																cssStyle="height:250px;width:375px;" />
															<p align="center">
																<span id="box7Counter" class="countLabel"></span>
															</p>
															<select style="display: none;" id="box7Storage"></select>
														</div>
												</td>
												<td style="padding-top: 5%">
													<div class="mws-dualbox-col2">
														<button id="to8" type="button" title="Move Selected Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-right"></i>
														</button>
														<br /> <br />
														<button id="allTo8" type="button" title="Move All Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-double-right"></i>
														</button>
														<br /> <br />
														<div class="clear"></div>
														<button id="to7" type="button" title="Move Selected Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-left"></i>
														</button>
														<br /> <br />
														<button id="allTo7" type="button" title="Move All Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-double-left"></i>
														</button>
													</div>
												</td>
												<td>
													<div class="mws-dualbox clearfix">
														<div class="mws-dualbox-col1">
															<div class="mws-dualbox-filter clearfix">
																<p align="center">
																	<label for="box8Filter"> Assigned Insights Data
																		Sources Groups</label> <input id="box8Filter"
																		class="form-control" type="text">
															</div>
															<s:select id="box8View"
																list="listInsightDatasourceGroup2"
																name="insightDatasourceGroupIds" listKey="id"
																listValue="dgName" cssClass="form-control"
																multiple="true" size="15"
																cssStyle="height:250px;width:375px;" />
															<p align="center">
																<span id="box8Counter" class="countLabel"></span>
															</p>
															<select style="display: none;" id="box8Storage"></select>
														</div>
													</div>
													</div>
												</td>
											</tr>
										</table>
									</div>
								</div>

								<div class="form-group m-form__group row" id="roles3">
									<label class="col-lg-2 control-label">Roles<span
										class="required" style="color: red;"> * </span></label>
									<div class="col-lg-4" align="left">
										<span class="m-form__help" id="roleHelp"
											style="color: red; align: center;"></span>
										<table
											class="form-horizontal form-bordered form-label-stripped">
											<tr>
												<td>
													<div class="mws-dualbox clearfix">
														<div class="mws-dualbox-col1">
															<div class="mws-dualbox-filter clearfix">
																<p align="center">
																	<label for="box5Filter" align="center">Available
																		Roles</label> <input id="box5Filter" class="form-control"
																		type="text">
															</div>
															<s:select id="box5View" list="listRole1" listKey="roleId"
																listValue="roleName" name="roleId"
																cssClass="form-control" multiple="true" size="15"
																cssStyle="height:250px;width:375px;" />
															<p align="center">
																<span id="box5Counter" class="countLabel"></span>
															</p>
															<select style="display: none;" id="box5Storage"></select>
														</div>
													</div>
												</td>
												<td style="padding-top: 5%">
													<div class="mws-dualbox-col2">
														<button id="to6" type="button" title="Move Selected Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-right"></i>
														</button>
														<br /> <br />
														<button id="allTo6" type="button" title="Move All Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-double-right"></i>
														</button>
														<br /> <br />
														<div class="clear"></div>
														<button id="to5" type="button" title="Move Selected Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-left"></i>
														</button>
														<br /> <br />
														<button id="allTo5" type="button" title="Move All Items"
															style="width: 35px; -webkit-appearance: caret;">
															<i style="color: gray;" class="fa fa-angle-double-left"></i>
														</button>
													</div>
												</td>
												<td>
													<div class="mws-dualbox clearfix">
														<div class="mws-dualbox-col1">
															<div class="mws-dualbox-filter clearfix">
																<p align="center">
																	<label for="box6Filter"> Assigned Roles</label> <input
																		id="box6Filter" class="form-control" type="text">
															</div>
															<s:select id="box6View" list="listRole2" name="roleIds"
																listKey="roleId" listValue="roleName"
																cssClass="form-control" multiple="true" size="15"
																cssStyle="height:250px;width:375px;" />
															<p align="center">
																<span id="box6Counter" class="countLabel"></span>
															</p>
															<select style="display: none;" id="box6Storage"></select>
														</div>
													</div>
												</td>
											</tr>
										</table>
									</div>
								</div>	
									
									<!-- <div class="form-actions" > -->
										<div class="row">
											<div class="col-lg-4"></div>
											<div class="col-lg-6"  style="margin-left:80px;">
											 <button type="button" id="save1"   class="btn btn-outline-success btn-sm m-btn m-btn--icon m-btn--outline-2x">
													<i class="fa fa-save"></i> Save
												</button>
												&nbsp;
  <button type="button" onclick="javascript:refreshForm();return false;" class="btn btn-outline-danger m-btn btn-sm  m-btn--icon m-btn--outline-2x"
												>Cancel</button>
												
												
													
											</div>
										</div>									
							

							
							</div>
						</div>
						</s:form>
						
				</section>
						
									</div></div>	
									<!-- <div class="modal-footer"></div> -->
									
									
								
							</div>	</div>
							
	</div>						
							
							
							</div>
						</div>

						<!--end::Modal-->	
		
		
		
			<div class="modal fade" id="m_modal_RESET"  role="dialog" 	aria-labelledby="exampleModalLabel" aria-hidden="true"  style=" overflow: auto !important;" ><!-- style="width: 56%; margin-left: -28%;" -->
						
						
						
							<div class="modal-dialog modal-lg " 	 role="document"  style="  display:contents !important; position: relative !important;    pointer-events: none !important;"> <!-- style="width: 56%;  margin: auto;" -->
	<div class="modal-content" style=" margin-top: 10%;">
	
	<div class="m-portlet m-portlet--mobile portPad">	
	
									<div class="modal-header">
									<div>
									 
									<h3 class="modal-title m-portlet__head-text m--font-transform-u" id="exampleModalLabel">
									<span class="m-portlet__head-icon"> <i class="fa fa-cogs"></i>
									</span><label id="resetHead"></label>
										</h3></div>
										 <div id="" class="" style="float:right !important; "><a  onclick="javascript:refreshReset();"><i class="fas fa-times fa-3x" ></i></a> </div>
										
									</div>
									<div class="modal-body" style="  WIDTH: 103%;  MAX-WIDTH: 1000PX;">
															<div class="resetForm">
									
									<s:form role="form"  method="post"
								id="user-form"  autocomplete="off"
								cssClass="form-horizontal form-bordered form-row-stripped">
									<div class="m-portlet__body form">
									<s:hidden name="message" id="message"></s:hidden>
								<s:hidden name="messagetype" id="messagetype"></s:hidden>
								<s:hidden name="userId" value="%{userId}" id="userId" />
								<s:hidden value="%{users.timeZone}" id="setTimeZone" />
								<s:hidden name="status" value="%{users.status}"></s:hidden>
								<s:hidden name="accountId" value="%{users.accountId}"></s:hidden>
								<s:hidden name="name" value="%{users.name}"></s:hidden>
								<s:hidden name="admin" value="%{users.admin}"></s:hidden>
						<%-- 		<s:push value="users"> --%>
									<%-- <s:if test="authType != 'ldap'">
									
										<s:if test="%{userId ==0}"> --%>
									
								
								<div class="form-group m-form__group row" id="user-nameBlock">
												<label class="col-lg-4 control-label">User Name</label>
												<div class="col-lg-6">															
													<s:textfield theme="simple" id="user-name" readonly="true"
														name="name" value="%{users.name}"
														cssClass="form-control m-input" />
											<%-- 	<span class="m-form__help" id="LoginIdHelp"></span>		 --%>	
																																																
												</div>
											</div>
											<div class="form-group m-form__group row" id="Reset-Pass-word">
												<label class="col-lg-4 control-label">New Password<span class="required" style="color :red;"> * </span></label>
												<div class="col-lg-6">													
														<s:password theme="simple" id="PassR" name="password"
															cssClass="form-control m-input" />
												<span class="m-form__help" id="Reset-PasswordHelp" style="color: red;width: 270px;"></span>																																									
												</div>
											</div>
											<div class="form-group m-form__group row" id="Reset-Reenterpass-word">
												<label class="col-lg-4 control-label">Re-type New
													Password<span class="required" style="color :red;"> * </span></label>
												<div class="col-lg-6">																											
															<s:password theme="simple" id="RepassR"
															name="reenterpassword"
															cssClass="form-control m-input" />
												<span class="m-form__help" id="Reset-RePasswordHelp" style="color: red;"></span>																																									
												</div>
											</div>
								
								<%-- </s:if>
									</s:if> --%>
									<%-- </s:push> --%>
									
									
											<div class="form-actionsRESET">
												<div class="row">
													<div class="col-lg-4"></div>
													<div class="col-lg-6" id="SavCan"  style="top: 20px;left: 10px;">														
													
									<button type="button" id="resetSave"  class="btn btn-outline-success btn-sm m-btn m-btn--icon m-btn--outline-2x">
													<i class="fa fa-save"></i> Save
												</button>
												&nbsp; <button type="button"  onclick="javascript:refreshReset();" class="btn btn-outline-danger m-btn btn-sm  m-btn--icon m-btn--outline-2x"
												>Cancel</button>			
													
													<%-- <s:submit label="Save" theme="simple" id="reset-password"
													cssClass="btn btn-outline-success btn-sm m-btn m-btn--icon m-btn--outline-2x" type="button" />&nbsp;												
													
													<a class="btn btn-outline-danger m-btn btn-sm  m-btn--icon m-btn--outline-2x"
													href="list-admin-ir-user.voziq"><s:text name="Cancel" /></a> --%>
													</div>
												</div>
											
										</div>
										</div>
									</s:form>
									</div>	
									</div>
									<!-- <div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-primary">Send message</button>
									</div> -->
								</div>
							</div>
						</div>
		</div>
		
		

		
<s:include value="/WEB-INF/jsp/global/list-footer.jsp" />